## 15puzzle console game on scala:

**requirements**:
sbtVersion 1.3.9


**simply:**

- `sbt test`
- `sbt run`

see smthing like: 
![](img/screen.png)


used this proof to avoid unsolvable cases:
https://mathworld.wolfram.com/15Puzzle.html
