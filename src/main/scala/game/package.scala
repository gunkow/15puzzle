package object game {
  type Board = Vector[Vector[Option[Int]]]

  type KeyMap[T] = Map[T, Action]

  val finishBoard: Board = Range(1, 17).toVector.sliding(4, 4).toVector.map(_.map(i => if (i < 16) Some(i) else None))
}
