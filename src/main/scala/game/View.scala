package game

trait View {
  def render(state: State): String
  def report(message: String): Unit
}

object SimpleView extends View {
  override def render(state: State): String = {
    state.getBoard.map(_.map {
      case Some(i) => " " + i.toString + (if (i > 9) "" else " ")
      case None => " \uD83E\uDDC5️"
    }.
      mkString(" | ")
    ).
      toList.mkString("\n").prepended('\n')

  }
  def report(message: String): Unit = println(message)
}
