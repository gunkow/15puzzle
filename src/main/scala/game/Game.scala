package game

abstract class Game(var state: State, view: View) {

  def cycle(input: Action): Unit

  def isFinish: Boolean = state.isFinish

  def report(message: String): Unit = view.report(message)
}

class StdGame(initState: State, view: View) extends Game(initState, view) {
  view.report("Starting game 15puzzle...\n")
  view.report(view.render(initState))

  def cycle(action: Action): Unit = {
    val newstate = state.move(action)
    if (newstate == state)
      view.report("No way\uD83E\uDD10")
    state = newstate
    view.report(view.render(state))
    if (state.isFinish) {
      view.report("Congrats! You won!")
    } else {
      view.report("Your turn...")
    }
  }

}







