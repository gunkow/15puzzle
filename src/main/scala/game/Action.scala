package game

sealed trait Action

case object Up extends Action

case object Down extends Action

case object Left extends Action

case object Right extends Action

