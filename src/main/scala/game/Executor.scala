package game


abstract class absExecutor[T](game: Game, keyMap: KeyMap[T]) {
  def exec(f: => Unit): Unit = f
}

class Executor(puzzleGame: Game, keyMap: KeyMap[Char]) extends absExecutor(puzzleGame, keyMap) {
  puzzleGame.report("\nType:  w/a/s/d  or  Esc  to abort↵... ")
}