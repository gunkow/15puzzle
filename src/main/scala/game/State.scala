package game

import scala.util.Random


class State(board: Board) {

  def getBoard: Board = board

  def getXY: (Int, Int) = {
    val r: Vector[(Int, Int)] = (for {row <- board.zipWithIndex
                                     cell <- row._1.zipWithIndex
                                     if cell._1.isEmpty
                                     } yield (cell._2, row._2))
    r(0)
  }

  def move(act: Action): State = act match {
    case Up => getXY match {
      case (_, j) if j == 0 => this
      case (i, j) => swap((i, j), (i, j - 1))
    }
    case Down => getXY match {
      case (_, j) if j == 3 => this
      case (i, j) => swap((i, j), (i, j + 1))
    }
    case Left => getXY match {
      case (i, _) if i == 0 => this
      case (i, j) => swap((i, j), (i - 1, j))
    }
    case Right => getXY match {
      case (i, _) if i == 3 => this
      case (i, j) => swap((i, j), (i + 1, j))
    }
  }

  private def swap(xy1: (Int, Int), xy2: (Int, Int)): State = {
    val result = board.map(_.toArray).toArray
    result(xy1._2)(xy1._1) = board(xy2._2)(xy2._1)
    result(xy2._2)(xy2._1) = board(xy1._2)(xy1._1)
    new State(result.map(_.toVector).toVector)
  }

  def isFinish: Boolean = {
    this == new State(finishBoard)
  }

  def equals(oth: State): Boolean = {
    val zip = this.board zip oth.getBoard
    zip forall { case (a, b) => a sameElements (b) }
  }

  override def equals(obj: Any): Boolean = obj match {
    case o: State => this.equals(o)
    case _ => super.equals(obj)
  }


  /** https://mathworld.wolfram.com/15Puzzle.html */
  def isUnsolvable: Boolean = {
    val unsummed =
      for {
        i <- 0 to 3
        j <- 0 to 3
      } yield
        (for {
          ii <- i to 3
          jj <- j to 3
          if board(jj)(ii).getOrElse(16) < board(j)(i).getOrElse(0)
        } yield 1).length

    return (unsummed.sum + getXY._2) % 2 == 0
  }
}

object State {

  @scala.annotation.tailrec
  def randomState(seed: Int = Random.nextInt()): State = {
    Random.setSeed(seed)

    val shuffled: Vector[Option[Int]] = Random.shuffle(game.finishBoard.flatten)
    val state = new State(shuffled.sliding(4, 4).toVector)
    if (state.isUnsolvable)
      randomState(Random.nextInt())
    else
      state
  }

  def apply(board: Board): State = new State(board)
}
