import java.awt.event.{KeyAdapter, KeyEvent}

import javax.swing.{JFrame, SwingUtilities}
import game.{Action, Executor, KeyMap, SimpleView, State, StdGame}
import game.{Down, Left, Right, Up}

object Main extends App {
  val puzzleGame = new StdGame(State.randomState(), SimpleView)
  val keyMap: KeyMap[Char] = Map('w' -> Up, 'a' -> Left, 's' -> Down, 'd' -> Right)

  val fr = new JFrame
  fr.setFocusable(true)
  fr.setVisible(true)
  fr.setSize(200, 200)
  fr.setEnabled(true)

  val executor = new Executor(puzzleGame, keyMap)

  executor exec {
    fr.addKeyListener(new KeyAdapter() {
      override def keyPressed(e: KeyEvent): Unit = e match {
        case e if puzzleGame.isFinish || e.getKeyCode == KeyEvent.VK_ESCAPE =>
          fr.dispose()
          System.exit(0)
        case event => keyMap.get(event.getKeyChar) match {
          case Some(act) => puzzleGame.cycle(act)
          case None => puzzleGame.report("unknown char")
        }
      }
    })
  }


}