import game.{Down, Game, KeyMap, Left, Right, SimpleView, State, StdGame, Up}
import org.scalatest._

import scala.util.{Failure, Success}

class Speca extends FlatSpec with Matchers {

  val keyMap: KeyMap = Map('w' -> Up, 'a' -> Left, 's' -> Down, 'd' -> Right)

  "State" should "be good boy" in {
    val startState = State(game.finishBoard)
    var state = startState.move(Down)

    assert(startState == state)

    state = state.move(Up)

    assert(startState != state)

    state = state.move(Left)
    state = state.move(Down)
    state = state.move(Right)
    state = state.move(Left)
    state = state.move(Up)
    state = state.move(Right)
    state = state.move(Down)


    assert(startState == state)

  }

  it should "win" in {
    var state = State(game.finishBoard)
    state = state.move(Up)
    val iter: Iterator[Char] = List('a', 'd', 's').iterator
    val puzzleGame: Game = new StdGame(state, SimpleView)
    while (iter.hasNext)
      puzzleGame.cycle(keyMap(iter.next()))

    assert(puzzleGame.isFinish)

  }

  it should "fail" in {
    var state = State(game.finishBoard)
    state = state.move(Up)
    val iter: Iterator[Char] = List('a', 's', 'd', 'd', 's').iterator
    val puzzleGame: Game = new StdGame(state, SimpleView)
    while (iter.hasNext)
      puzzleGame.cycle(keyMap(iter.next()))

    assert(!puzzleGame.isFinish)


  }
}